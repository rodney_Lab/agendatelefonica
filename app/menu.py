
def opciones(agenda):
    exit=0

    db_selected=0
    #Configuracion base de datos seleccionada
    f=open("config.txt","r")
    ln=f.read()
    l1=ln.split("=")
    if l1[1]=="mongo":
        db_selected=0
    if l1[1]=="sql":
        db_selected=1
    f.close()

    while exit==0:
        print(":::::::::::::::::::::::::::::")
        if db_selected==0:
            print("Base de datos: MongoDB")
        if db_selected==1:
            print("Base de datos: MySQL")
        print("Agenda Telefonica")
        print("1: Agregar contacto")
        print("2: Listar contato")
        print("3: Buscar contato")
        print("4: Editar contato")
        print("5: Eliminar contato")
        print("6: Cambiar de base de datos")
        print("7: Salir")
        opcion=int(input("Seleccione una opcion:  "))

        if opcion==1:
            nombre=input("Nombre: ")
            telefonoC=input("Telefono celular: ")
            telefonoF=input("Telefono fijo: ")
            email=input("Email: ")
            fecha=input("Fecha nacimiento: ")
            direccion=input("Direccion: ")
            foto=input("Foto: ")

            #Base de datos mongodb
            if db_selected==0:
                agenda.insertarContacto(nombre,telefonoC,telefonoF,email,fecha,direccion,foto)
            #Base de datos sql
            if db_selected==1:
                print("insertar contacto sql")
            
        if opcion==2:
            #Base de datos mongodb
            if db_selected==0:
                agenda.listarContacto()
            #Base de datos sql
            if db_selected==1:
                print("Listar contacto sql")
            
        if opcion==3:
            con_buscar=input("Nombre del contacto:  ")

            #Base de datos mongodb
            if db_selected==0:
                agenda.buscarContacto(con_buscar)
            #Base de datos sql
            if db_selected==1:
                print("Buscar contacto sql")
            

        if opcion==4:
            #editar
            nombre_old=input("Nombre Antiguo: ")
            nombre=input("Nombre: ")
            telefonoC=input("Telefono celular: ")
            telefonoF=input("Telefono fijo: ")
            email=input("Email: ")
            fecha=input("Fecha nacimiento: ")
            direccion=input("Direccion: ")
            foto=input("Foto: ")

            #Base de datos mongodb
            if db_selected==0:
                agenda.editarContacto(nombre_old,nombre,telefonoC,telefonoF,email,fecha,direccion,foto)
            #Base de datos sql
            if db_selected==1:
                print("Editar contacto sql")
                
        if opcion==5:
            #eliminar
            nombre_contacto_eliminar=input("Nombre contacto a eliminar: ")
            #Base de datos mongodb
            if db_selected==0:
                agenda.eliminarContacto(nombre_contacto_eliminar)
            #Base de datos sql
            if db_selected==1:
                print("Eliminar contacto sql")
            
        if opcion==6:
            #Seleccionar base de datos
            print("Seleccione la base de datos")
            print("1: MongoDB")
            print("2: SQL")
            opc=int(input("Seleccione una opcion:  "))
            f=open("config.txt","w")
            if opc==1:
                f.write("db_select=mongo")  
                db_selected=0
            if opc==2:
                f.write("db_select=sql") 
                db_selected=1
            f.close()
        if opcion==7:
            exit=1