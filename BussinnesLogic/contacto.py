import sys
sys.path.append('./DataAccess')
from conexionMongo import mng

class agenda(mng):

    def __init__(self,conexion_mng):
        self.con_mng=conexion_mng
    def insertarContacto(self,nombre,telefono_cell,telefono_fijo,email,fecha_nac,direccion,foto):
        contacto={"Nombre":nombre,"Telf_cell":telefono_cell,"Telf_fijo":telefono_fijo,"Email":email,"Fecha_nac":fecha_nac,"Direccion":direccion,"Foto":foto}
        self.con_mng.mycol.insert_one(contacto)
    def listarContacto(self):
        for x in self.con_mng.mycol.find():
            print(x)
    def buscarContacto(self,nombre):
        myquery = { "Nombre": nombre }
        mydoc = self.con_mng.mycol.find(myquery)
        print("buscar contacto")
        for x in mydoc:
            print(x)
    def editarContacto(self,nombre_old,nombre,telefono_cell,telefono_fijo,email,fecha_nac,direccion,foto):
        myquery = { "Nombre": nombre_old }
        contacto={"Nombre":nombre,"Telf_cell":telefono_cell,"Telf_fijo":telefono_fijo,"Email":email,"Fecha_nac":fecha_nac,"Direccion":direccion,"Foto":foto}
        newvalues = { "$set": contacto }
        self.con_mng.mycol.update_one(myquery,newvalues)
    def eliminarContacto(self,contacto_eliminar):
        myquery = { "Nombre": contacto_eliminar}
        self.con_mng.mycol.delete_one(myquery)

